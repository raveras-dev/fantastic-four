 /*VARIAVEIS*/

const jogador1Name = document.getElementById("jogador1");
const scoreJogador1 = document.getElementById("score1");
const jogador2Name = document.getElementById("jogador2");
const scoreJogador2 = document.getElementById("score2");
const buttonEl = document.getElementById("start")
const gameContainer = document.getElementById("table-container");

let board;
let form = document.querySelector("form");
let introBox = document.querySelector("#intro");
let p1Name = document.querySelector("#playerOne");
let p2Name = document.querySelector("#playerTwo");
let playerOne;
let playerTwo;
let scorePlayer1 = 0;
let scorePlayer2 = 0;
createGame();


form.addEventListener("submit", (e) => {
	e.preventDefault();
	
    if (p1Name.value == "") {
		document.querySelector('#p1Error').style.opacity = "1";
		p1Name.focus();
	} else if (p2Name.value == "") {
		document.querySelector('#p2Error').style.opacity = "1";
		p2Name.focus();
	} else {
        playerOne = p1Name.value;
        playerTwo = p2Name.value;
		setTimeout(() => introBox.style.display = "none", 300);
	}
    
    construirScore(playerOne, playerTwo);
});

function createGame(){
    document.getElementById('final').classList.add('hidden');
    document.querySelector('.modal_filho').classList.add('hidden');

    gameContainer.innerHTML = "";
    let playsCount = 0;

    board = [
        [0, 0, 0, 0, 0, 0], 
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ];

    createTable(board);
    let discoAtual = mostraDiscoAtual();
    let redToken = true;
    //createTokens(redToken);

    const columnsArr = document.querySelectorAll(".column");
    columnsArr.forEach((column, index) => {
        column.addEventListener("click", () =>{
            
            for(let i = column.children.length - 1; i >= 0; i--){   
                let linhaAtual = column.children[i];
                
                if(linhaAtual.classList.contains("empty")){
                    playsCount++;

                    linhaAtual.classList.remove("empty")   
                    linhaAtual.children[0].appendChild(createTokens(redToken))
                    
                    discoAtual.classList.toggle("disco_vermelho");

                    if(redToken){
                        board[index][i] = 1;
                    } else {
                        board[index][i] = 2;
                    }

                    redToken = !redToken;       
                    break; 
                }
             }

            if(checkVictory(board)){
                let jogador = "laranja";

                if(!redToken){
                    jogador = "vermelho";
                    scorePlayer1++;
                    mudarScore(scorePlayer1,scorePlayer2);

                } else {
                    scorePlayer2++;
                    mudarScore(scorePlayer1,scorePlayer2);
                }

                telaFinal(jogador);

            }else if (playsCount === 42) {
 
                telaFinal();
            }
        })  
    })

}

function createTable(board){
    for(let i = 0; i < board.length; i++){
        const columnEl = document.createElement("div");
        columnEl.classList.add(`column`);

        for(let j = 0; j < board[i].length; j++){
            const line = document.createElement("div");
            const circle = document.createElement("div");
            circle.classList.add("circle");
            
            line.classList.add(`cell`);
            line.classList.add("empty");
            line.classList.add(`line${j}`);

            line.appendChild(circle)
            columnEl.appendChild(line);
        }
        gameContainer.appendChild(columnEl);
    }
}

function createTokens(redToken){
    let createToken = document.createElement("div");
    createToken.classList.add("circle");
    
        if(redToken){
            createToken.setAttribute("id","fichaVermelha");
    
        } else {
            createToken.setAttribute("id","fichaLaranja");
        }

    return createToken
}

function construirScore(playerOne, playerTwo) {
        //inicializando o score
        jogador1Name.innerHTML = "";    
        jogador2Name.innerHTML="";
        scoreJogador1.innerHTML = "";
        scoreJogador2.innerHTML = "";

        let conteudoNomeLeft = document.createTextNode(`${playerOne}`)
        
        jogador1Name.appendChild(conteudoNomeLeft)
        let conteudoScore1 = document.createTextNode(`${scorePlayer1}`)
        scoreJogador1.appendChild(conteudoScore1)

        let conteudoNomeRight = document.createTextNode(`${playerTwo}`)

        jogador2Name.appendChild(conteudoNomeRight)
        let conteudoScore2 = document.createTextNode(`${scorePlayer2}`)
        scoreJogador2.appendChild(conteudoScore2)
}

function mudarScore(score1,score2){
    const novoScore1 = document.createTextNode(`${score1}`)
    const novoScore2 = document.createTextNode(`${score2}`)
    scoreJogador1.innerHTML = '';
    scoreJogador1.appendChild(novoScore1);
    scoreJogador2.innerHTML = '';
    scoreJogador2.appendChild(novoScore2);
}


function checkVictory(board){
    if(checkVertical(board) || checkHorizontal(board) ||  checkDiagonal(board)){
        return true
    }
    return false
}

function checkVertical(board) {
    const limX = board[0].length - 3;
    let isWin = false;
    
    for (let y = 0; y < board.length; y++) {

        for (let x = 0; x < limX; x++) {
            let cell = board[y][x];

            if (cell !== 0) {

                if (cell === board[y][x+1] && 
                    cell === board[y][x+2] && 
                    cell === board[y][x+3]) {

                    isWin = true;

                    let lin = [x,x+1,x+2,x+3];
                    let col = [y];
                    winEffect(col,lin,false,true);

                    return isWin;  
                }         
            }
        }
    }
    return isWin;
};

function checkHorizontal(board) {
    const limY = board.length - 3;
    let isWin = false;

    for (let y = 0; y < limY; y++) {

        for (let x = 0; x < board[0].length; x++) {
            let cell = board[y][x];

            if (cell !== 0) {

                if (cell === board[y+1][x] && 
                    cell === board[y+2][x] && 
                    cell === board[y+3][x]) {

                    isWin = true;

                    let lin = [x];
                    let col = [y,y+1,y+2,y+3];
                    winEffect(col,lin,true,false);

                    return isWin;
                }
            }
        }
    }
    return isWin;
}

function checkDiagonal(board){
    let limiteX = board[0].length -3; 
    let limiteY = board.length - 3;
    let cell;
    let isWin = false; 

    for(let y = 0; y < limiteY; y++){

        for(let x = 0; x<limiteX; x++){
            cell = board[y][x];

            if(cell !== 0){
               
                if(
                    cell === board[y+1][x+1] &&
                    cell === board[y+2][x+2] &&
                    cell === board[y+3][x+3]
                    ){
                        isWin = true;

                        let lin = [x,x+1,x+2,x+3];
                        let col = [y,y+1,y+2,y+3];
                        winEffect(col,lin,true,true);

                        return isWin;
                     }
            }
        }
        
    }
    
    for(let y = 3; y < board.length; y++){
        
        for(let x = 0; x < limiteX; x++){
            cell = board[y][x];

            if(cell !== 0){
                if(
                    cell === board[y-1][x+1] &&
                    cell === board[y-2][x+2] &&
                    cell === board[y-3][x+3]
                    ){
                        isWin = true;

                        let lin = [x,x+1,x+2,x+3];
                        let col = [y,y-1,y-2,y-3];
                        winEffect(col,lin,true,true);

                        return isWin;
                     }
            }
        }
    }
    return isWin;
}

function mostraDiscoAtual(){
    const apoio = document.querySelector('.holding__token');
    apoio.innerHTML = "";
  
    const msg = document.createElement('p');
    msg.innerText = "agora é a vez do";

    const disco = document.createElement('div');
    disco.classList.add("disco",'disco_vermelho');

    apoio.appendChild(msg);
    apoio.appendChild(disco);
    
    return disco;
}

function telaFinal(jogador = ""){
    const tela = document.getElementById('final');
    const resultado = document.getElementById('resultado');
    const botao = document.getElementById('resetar');

    if(jogador.length < 1){
        resultado.innerText = "EMPATE!";

    } else {
        confetti({
            particleCount: 100,
            startVelocity: 30,
            spread: 360,
          });

        if(jogador === "vermelho"){
        resultado.innerText = `${playerOne} GANHOU!!!`;      
        }
        else {
            resultado.innerText = `${playerTwo} GANHOU!!!`;
          }
    }   
    tela.classList.remove('hidden');
    
    setTimeout(function() { 
        document.querySelector('.modal_filho').classList.remove('hidden');
        ; }, 500);

    botao.addEventListener('click', createGame, false); 
}

function winEffect(arrCol, arrLin, hor,vert){
    let coluna;
    let celula;
    let elemCol = document.querySelectorAll('.column');

    if (hor && vert) {
        for(let i = 0; i < 4; i++){
            elemCol[arrCol[i]].children[arrLin[i]].lastChild.classList.add('efeito');
        }

    } else if(hor) {
        celula = arrLin[0];
        for(let i = 0; i < 4; i++){
            elemCol[arrCol[i]].children[celula].lastChild.classList.add('efeito');      
        }

    } else if(vert) {
        coluna = arrCol[0];
        for(let i = 0; i < 4; i++){
            elemCol[coluna].children[arrLin[i]].lastChild.classList.add('efeito');
        }
    }
}
